﻿#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <tuple>

using times_table = std::vector<std::vector<std::chrono::duration<double>>>;

using part_for_thread = std::pair<int, int>;
using parts_for_threads = std::vector<part_for_thread>;

//TASK_1
///////////////////////////////////////////////////////////////////////////////////////
void safe_inc_func(int thread_num, int& count, std::mutex& inc_mutex) {
    std::lock_guard<std::mutex> lock(inc_mutex);
    if (count > 100) return;
    std::cout << "Thread number " << thread_num << " is working\n";
    count += thread_num;
    std::cout << "New count value: " << count << "\n";
}

void task_1() {
    std::cout << "Input number of threads: ";
    int n; std::cin >> n;
    auto count = 0;
    std::mutex inc_mutex;
    auto threads = std::vector<std::thread>(n);
    while (count < 100) {
        for (int i = 0; i < n; i++) {
            threads[i] = std::thread(safe_inc_func, i + 1, std::ref(count), std::ref(inc_mutex));
        }
        for (int i = 0; i < n; i++)
            threads[i].join();
    }
}

//TASK_2
///////////////////////////////////////////////////////////////////////////////////////
const auto NUM_OF_REP = 10;
auto SIZES = std::vector<size_t>() = { (size_t)5e7, (size_t)5e8, (size_t)2e9 };
auto NUMS_OF_THREADS = std::vector<int>() = { 1, 2, 4, 8 };

long long sum = 0;
std::mutex sum_mutex;

void print_table(const times_table& table) {
    for (auto row : table) {
        for (auto cell : row)
            std::cout << cell.count() << " ";
        std::cout << "\n";
    }
}

times_table get_accelerations(const times_table& times) {
    auto num_of_rows = times.size();
    auto num_of_columns = times.front().size();
    auto accelerations = times_table(num_of_rows);
    for (size_t i = 0; i < num_of_rows; i++)
        for (size_t j = 1; j < num_of_columns; j++)
            accelerations[i].push_back((std::chrono::duration<double>)(times[i][0] / times[i][j]));
    return accelerations;
}

void average_times(times_table& times) {
    auto num_of_rows = times.size();
    auto num_of_columns = times.front().size();
    for (size_t i = 0; i < num_of_rows; i++)
        for (size_t j = 0; j < num_of_columns; j++)
            times[i][j] /= NUM_OF_REP;
}

void update_time(times_table& times, int i, int j, std::chrono::duration<double> delta_time) {
    times[i][j] += delta_time;
}

long long get_hash(int size) {
    return size * (1 + (long long)size) / 2;
}

bool check_arr_hash(int size) {
    return get_hash(size) == sum;
}

void join_threads(std::vector<std::thread>& threads) {
    for (size_t i = 0; i < threads.size(); i++)
        threads[i].join();
}

void sum_func(const part_for_thread& part) {
    long long cur_sum = 0;
    for (int i = part.first; i <= part.second; i++)
        cur_sum += i;
    std::lock_guard<std::mutex> lock(sum_mutex);
    sum += cur_sum;
}

auto get_now() {
    return std::chrono::high_resolution_clock::now();
}

auto sum_sequence(const parts_for_threads& parts, std::vector<std::thread> &threads) {
    auto start_time = get_now();
    for (size_t i = 0; i < threads.size(); i++)
        threads[i] = std::thread(sum_func, ref(parts[i]));
    join_threads(threads);
    auto end_time = get_now();
    return end_time - start_time;
}

void shift_parts(parts_for_threads& parts, int num_of_parts, int num_of_res) {
    for (int i = 1; i < num_of_parts; i++) {
        if (num_of_res >= i) {
            parts[i - 1].second += i;
            parts[i].first += i;
        }
        else {
            parts[i - 1].second += num_of_res;
            parts[i].first += num_of_res;
        }
    }
    (*(prev(parts.end()))).second += num_of_res;
}

parts_for_threads get_parts(int size, int num_of_parts, int part_size) {
    auto parts = std::vector<std::pair<int, int>>();
    auto start_part_value = 1;
    for (int i = 0; i < num_of_parts; i++) {
        auto end_part_value = start_part_value + part_size - 1;
        parts.push_back({ start_part_value, end_part_value });
        start_part_value = end_part_value + 1;
    }
    return parts;
}

std::tuple<int, int> get_part_size(size_t size, int num_of_parts) {
    return { size / num_of_parts, size % num_of_parts };
}

parts_for_threads split_sequence(int size, int num_of_parts) {
    auto [part_size, num_of_res] = get_part_size(size, num_of_parts);
    auto parts = get_parts(size, num_of_parts, part_size);
    if (num_of_res > 0)
        shift_parts(parts, num_of_parts, num_of_res);
    return parts;
}

times_table measure_times(bool check_res) {
    auto times = times_table
    (SIZES.size(), std::vector<std::chrono::duration<double>>(NUMS_OF_THREADS.size()));
    for (size_t i = 0; i < SIZES.size(); i++) {
        for (size_t j = 0; j < NUMS_OF_THREADS.size(); j++) {
            auto parts = split_sequence(SIZES[i], NUMS_OF_THREADS[j]);
            auto threads = std::vector<std::thread>(NUMS_OF_THREADS[j]);
            for (int k = 1; k <= NUM_OF_REP; k++) {
                auto time = sum_sequence(parts, threads);
                if (check_res && !check_arr_hash(SIZES[i])) throw std::exception("Array hash is failed!");
                update_time(times, i, j, time);
                sum = 0;
            }
            std::cout << "Time is measured for N = " << SIZES[i] << " with the number of threads " << NUMS_OF_THREADS[j] << "\n";
        }
        std::cout << "\n";
    }
    return times;
}

void task_2() {
    auto times = measure_times(true);
    average_times(times);
    std::cout << "Times table:\n";
    print_table(times);
    auto accelerations = get_accelerations(times);
    std::cout << "\nAccelerations table:\n";
    print_table(accelerations);
}

int main()
{
    //task_1();
    try {
        auto start_time = get_now();
        task_2();
        auto finish_time = get_now();
        std::chrono::duration<double> run_time = finish_time - start_time;
        std::cout << "\nProgram is finished in " << run_time.count() << " sec.!\n";
    }
    catch (const std::exception & ex) {
        std::cout << ex.what();
    }
}


