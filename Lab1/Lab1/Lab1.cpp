﻿#include <iostream>
#include <stdarg.h>
#include <thread>
#include <array>
#include <functional>
#include <vector>
#include <tuple>
#include <numeric>
#include <algorithm>
using namespace std;

using times_table = vector<vector<chrono::duration<double>>>;

template<typename T>
using part_for_thread = pair<typename vector<T>::iterator, typename vector<T>::iterator>;

template<typename T>
using parts_for_threads = vector<part_for_thread<T>>;

//COMMON FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////
void print_message(string pattern, string end, ...)
{
    va_list args;
    va_start(args, end);
    for (auto format : pattern)
        switch (format) {
        case 'i': cout << va_arg(args, int); break;
        case 'd': cout << va_arg(args, double); break;
        case 's': cout << va_arg(args, const char*); break;
        case 'b': cout << va_arg(args, bool); break;
        default: break;
        }
    cout << end;
    va_end(args);
}

void print_thread_info(int thread_num, int rep_count, int sleep_time, bool print_start, bool print_end) {
    while (rep_count--) {
        (print_start) ? print_message("sis", "\n", "Thread number ", thread_num, " is started!") 
            : print_message("i", "\n", thread_num);
        this_thread::sleep_for(chrono::seconds(sleep_time));
        if (print_end)
            print_message("sis", "\n", "Thread number ", thread_num, " is finished!");
    }
}

void join_threads(vector<thread>& threads) {
    for (size_t i = 0; i < threads.size(); i++)
        threads[i].join();
}

//TASK_0
///////////////////////////////////////////////////////////////////////////////////////
void create_thread(bool joinable) {
    thread myThread;
    print_message("sb", "\n", "Before starting, joinable: ", myThread.joinable());

    print_message("s", "\n", "Start!");
    myThread = thread(print_thread_info, 1, 1, 3, true, true);
    print_message("sb", "\n", "After starting, joinable: ", myThread.joinable());

    (joinable) ? myThread.join()
        : myThread.detach();

    print_message("sb", "\n", "After finish, joinable: ", myThread.joinable());
}

void task_0() {
    print_message("s", "\n", "Thread with join:");
    create_thread(true);
    print_message("s", "\n", "\nThread without join:");
    create_thread(false);
}

//TASK_1
///////////////////////////////////////////////////////////////////////////////////////
const auto SIZE = 100;

void print_array(const array<int, SIZE> &arr, function<bool(int)> pred) {
    for (size_t i = 0; i < arr.size(); i++)
        if (pred(i))
            print_message("i", "\n", i);
}

void fill_array(array<int, SIZE>& arr) {
    for (size_t i = 0; i < arr.size(); i++)
        arr[i] = i + 1;
}

void task_1() {
    auto arr = array<int, SIZE>();
    fill_array(arr);

    auto even_pred = [](int i) {return i % 2 == 0; };
    auto odd_pred = [](int i) {return i % 2 != 0; };

    print_message("s", "\n", "Odd thread starts!");
    auto odd_thread = thread(print_array, arr, odd_pred);
    print_message("s", "\n", "Even thread starts!");
    auto even_thread = thread(print_array, arr, even_pred);

    odd_thread.join();
    even_thread.join();
}

//TASK_2
///////////////////////////////////////////////////////////////////////////////////////
void task_2() {
    print_message("s", " ", "Input number of threads:");
    int n; cin >> n;
    auto threads = vector<thread>(n);
    for (int i = 0; i < n; i++)
        threads[i] = thread(print_thread_info, i + 1, 5, 0, false, false);
    
    join_threads(threads);
}

//TASK_3
///////////////////////////////////////////////////////////////////////////////////////
void task_3() {
    print_message("s", " ", "Input number of threads:");
    int n; cin >> n;
    auto threads = vector<thread>(n);
    for (int i = 0; i < n; i++)
        threads[i] = thread(print_thread_info, i + 1, 1, 3, true, true);

    join_threads(threads);
}

//TASK_4
///////////////////////////////////////////////////////////////////////////////////////
auto const ARRAY_VALUE = 0; 
auto const NUM_OF_REP = 1;
auto ARRAY_SIZES = vector<size_t> {(size_t)1e6, (size_t)2e7, (size_t)5e7};
auto NUMS_OF_THREADS = vector<int> { 1, 2, 4, 8 };

void print_table(const times_table& table) {
    for (auto row : table) {
        for (auto cell : row)
            print_message("d", " ", cell.count());
        print_message("s", "\n", "");
    }
}

times_table get_accelerations(const times_table& times) {
    auto num_of_rows = times.size();
    auto num_of_columns = times.front().size();
    auto accelerations = times_table(num_of_rows);
    for (size_t i = 0; i < num_of_rows; i++)
        for (size_t j = 1; j < num_of_columns; j++)
            accelerations[i].push_back((chrono::duration<double>)(times[i][0] / times[i][j]));
    return accelerations;
}

void average_times(times_table& times) {
    auto num_of_rows = times.size();
    auto num_of_columns = times.front().size();
    for (size_t i = 0; i < num_of_rows; i++)
        for (size_t j = 0; j < num_of_columns; j++)
            times[i][j] /= NUM_OF_REP;
}

void clear_arr(vector<int>& arr, int value = 0) {
    fill(arr.begin(), arr.end(), value);
}

void update_time(times_table& times, int i, int j, chrono::duration<double> delta_time) {
    times[i][j] += delta_time;
}

int get_hash(const vector<int>& arr, int num_of_measure) {
    return accumulate(arr.begin(), arr.end(), 0) +
        count(arr.begin(), arr.end(), ARRAY_VALUE + num_of_measure);
}

bool check_arr_hash(const vector<int>& arr, int num_of_measure) {
    return get_hash(arr, num_of_measure) == arr.size() * (ARRAY_VALUE + num_of_measure + 1);
}

void inc_func(part_for_thread<int>& interval) {
    for (auto it = interval.first; it != interval.second; ++it)
        (*it)++;
}

auto get_now() {
    return chrono::high_resolution_clock::now();
}

auto inc_array(parts_for_threads<int>& parts, vector<thread> &threads) {
    auto start_time = get_now();
    for (size_t i = 0; i < threads.size(); i++)
        threads[i] = thread(inc_func, ref(parts[i]));
    join_threads(threads);
    auto end_time = get_now();
    return end_time - start_time;
}

template<typename T>
void shift_parts(parts_for_threads<T> &parts, int num_of_parts, int num_of_res) {
    for (int i = 1; i < num_of_parts; i++) {
        if (num_of_res >= i)
            parts[i - 1].second = parts[i].first += i;
        else
            parts[i - 1].second = parts[i].first += num_of_res;
    }
    (*(prev(parts.end()))).second += num_of_res;
}

template<typename T>
parts_for_threads<T> get_parts(vector<T>& arr, int num_of_parts, int part_size) {
    auto parts = parts_for_threads<T>();
    auto start_part_ptr = arr.begin();
    for (int i = 0; i < num_of_parts; i++) {
        auto end_part_ptr = start_part_ptr + part_size;
        parts.push_back({ start_part_ptr, end_part_ptr });
        start_part_ptr = end_part_ptr;
    }
    return parts;
}

tuple<int, int> get_part_size(size_t arr_size, int num_of_parts) {
    return { arr_size / num_of_parts, arr_size % num_of_parts };
}

template<typename T>
parts_for_threads<T> split_array(vector<T>& arr, int num_of_parts) {
    auto [part_size, num_of_res] = get_part_size(arr.size(), num_of_parts);
    auto parts = get_parts<T>(arr, num_of_parts, part_size);
    if (num_of_res > 0)
        shift_parts<T>(parts, num_of_parts, num_of_res);
    return parts;
}

times_table measure_times(bool check_res) {
    auto times = times_table
        (ARRAY_SIZES.size(), vector<chrono::duration<double>>(NUMS_OF_THREADS.size()));
    for (size_t i = 0; i < ARRAY_SIZES.size(); i++) {
        auto arr = vector<int>(ARRAY_SIZES[i], ARRAY_VALUE);
        for (size_t j = 0; j < NUMS_OF_THREADS.size(); j++) {
            auto parts = split_array<int>(arr, NUMS_OF_THREADS[j]);
            auto threads = vector<thread>(NUMS_OF_THREADS[j]);
            for (int k = 0; k < NUM_OF_REP; k++) {
                auto time = inc_array(parts, threads);
                if (check_res && !check_arr_hash(arr, k)) throw exception("Array hash is failed!");
                update_time(times, i, j, time);
            }
            print_message("sisi", "\n", "Time is measured for the array size ", ARRAY_SIZES[i], " with the number of threads ", NUMS_OF_THREADS[j]);
            clear_arr(arr, ARRAY_VALUE);
        }
        print_message("s", "\n", "");
    }
    return times;
}

void task_4() {
    auto times = measure_times(true);
    average_times(times);
    print_message("s", "\n", "Times table:");
    print_table(times);
    auto accelerations = get_accelerations(times);
    print_message("s", "\n", "\nAccelerations table:");
    print_table(accelerations);
}

//TASK5
///////////////////////////////////////////////////////////////////////////////////////
const int MATRIX_MAX_VALUE = 3;
const auto MATRIX_SIZES = vector<size_t>() = { 5000, 10000, 20000 };

auto seq_multiply_matrix(const vector<vector<int>> &matrix, const vector<int> &column, vector<int> &answer) {
    auto start_time = get_now();
    for (size_t i = 0; i < matrix.size(); i++)
        for (size_t j = 0; j < column.size(); j++)
            answer[i] += matrix[i][j] * column[j];
    auto end_time = get_now();
    return end_time - start_time;
}

void multiply_func(part_for_thread<vector<int>>& matrix_interval, const vector<int> column, part_for_thread<int> &answer_interval) {
    auto ans_it = answer_interval.first;
    for (auto row_it = matrix_interval.first; row_it != matrix_interval.second; ++row_it){
        for (size_t i = 0; i < (*row_it).size(); i++) {
            (*ans_it) += (*row_it)[i] * column[i];
        }
        ++ans_it;
    }
}

auto parallel_multiply_matrix(parts_for_threads<vector<int>>& matrix_parts, const vector<int>& column, parts_for_threads<int>& answer_parts, vector<thread> &threads) {
    auto start_time = get_now();
    for (size_t i = 0; i < threads.size(); i++)
        threads[i] = thread(multiply_func, ref(matrix_parts[i]), column, ref(answer_parts[i]));
    join_threads(threads);
    auto end_time = get_now();
    return end_time - start_time;
}

tuple<parts_for_threads<vector<int>>, parts_for_threads<int>> split_matrix(vector<vector<int>>& matrix, vector<int>& answer) {
    int num_of_parts = thread::hardware_concurrency();
    auto matrix_parts = split_array<vector<int>>(matrix, num_of_parts);
    auto answer_parts = split_array<int>(answer, num_of_parts);
    return make_tuple(matrix_parts, answer_parts);
}

vector<int> generate_vector(size_t n) {
    auto vector = std::vector<int>(n);
    for (size_t i = 0; i < n; i++)
        vector[i] = rand() % MATRIX_MAX_VALUE + 1;
    return vector;
}

vector<vector<int>> generate_matrix(size_t n){
    auto matrix = vector<vector<int>>(n, vector<int>(n));
    for (size_t i = 0; i < n; i++)
        for (size_t j = 0; j < n; j++)
            matrix[i][j] = rand() % MATRIX_MAX_VALUE + 1;
    return matrix;
}

times_table measure_times() {
    auto times = times_table
        (MATRIX_SIZES.size(), vector<chrono::duration<double>>(2));
    for (size_t i = 0; i < MATRIX_SIZES.size(); i++) {
        auto matrix = generate_matrix(MATRIX_SIZES[i]);
        auto column = generate_vector(MATRIX_SIZES[i]);
        auto answer = vector<int>(column.size(), 0);
        auto [matrix_parts, answer_parts] = split_matrix(matrix, answer);
        auto threads = vector<thread>(matrix_parts.size());
        for (int k = 0; k < NUM_OF_REP; k++) {
            auto seq_time = seq_multiply_matrix(matrix, column, answer);
            auto parallel_time = parallel_multiply_matrix(matrix_parts, column, answer_parts, threads);
            update_time(times, i, 0, seq_time); 
            update_time(times, i, 1, parallel_time);
            clear_arr(answer);
        }
    }
    return times;
}

void task_5() {
    srand((unsigned)time(0));
    auto times = measure_times();
    average_times(times);
    print_message("s", "\n", "Times table:");
    print_table(times);
    auto accelerations = get_accelerations(times);
    print_message("s", "\n", "\nAccelerations table:");
    print_table(accelerations);
}

//MAIN
///////////////////////////////////////////////////////////////////////////////////////
int main()
{
    //task_0();
    //task_1();
    //task_2();
    //task_3();
    try
    {
        auto start_time = get_now();
        task_4();
        //task_5();
        auto finish_time = get_now();
        chrono::duration<double> run_time = finish_time - start_time;
        print_message("sds", "", "\nProgram is finished in ", run_time.count(), " sec.!\n");
    }
    catch (const exception &ex)
    {
        print_message("s", "\n", ex.what());
    }
}

