#include "threads_measurer.h"

threads_measurer::threads_measurer(
	std::vector<size_t>::const_iterator arr_sizes_first, std::vector<size_t>::const_iterator arr_sizes_last,
	std::vector<size_t>::const_iterator nums_of_threads_first, std::vector<size_t>::const_iterator nums_of_threads_last,
	int num_of_rep,
	int max_value) : measurer(arr_sizes_first, arr_sizes_last, num_of_rep, max_value)
{
	nums_of_threads = std::vector<size_t>(nums_of_threads_first, nums_of_threads_last);
}

time_table threads_measurer::measure(const task<part_for_thread>& task)
{
	time_table times(arr_sizes.size(), nums_of_threads.size());

	for (size_t i = 0; i < arr_sizes.size(); i++)
		measure_for_current_size(task, times, arr_sizes[i], i);

	return times;
}

time_table threads_measurer::measure(const std::vector<task<std::vector<int>>>& tasks)
{
	return time_table(0, 0);
}

void threads_measurer::measure_for_current_size(const task<part_for_thread>& task, time_table &times, size_t size, int size_id)
{
	auto source_arr = std::vector<int>(size);
	auto result_arr = std::vector<int>(size);
	fill_arr(source_arr);
	fill_arr(result_arr);

	for (size_t i = 0; i < nums_of_threads.size(); i++)
		measure_for_current_num_of_threads(task, times, source_arr, result_arr, nums_of_threads[i], size_id, i);
}

void threads_measurer::measure_for_current_num_of_threads(const task<part_for_thread>& task, time_table& times, std::vector<int> &source_arr, std::vector<int>& result_arr, size_t num_of_threads, int size_id, int thread_id)
{
	array_splitter source_splitter(source_arr.begin(), source_arr.end());
	array_splitter result_splitter(result_arr.begin(), result_arr.end());

	auto source_arr_parts = source_splitter.split(num_of_threads);
	auto result_arr_parts = result_splitter.split(num_of_threads);
	auto threads = std::vector<std::thread>(num_of_threads);

	for (int i = 0; i < num_of_rep; i++) {
		auto delta_time = run_task(task, source_arr_parts, result_arr_parts, threads);
		times.update(size_id, thread_id, delta_time);
	}
}

parts_for_threads threads_measurer::split_array(std::vector<int>& arr, int num_of_parts)
{
	array_splitter splitter(arr.begin(), arr.end());
	auto arr_parts = splitter.split(num_of_parts);
	return arr_parts;
}

std::chrono::duration<double> threads_measurer::run_task(const task<part_for_thread>& task, const parts_for_threads& source_parts, parts_for_threads& result_parts, std::vector<std::thread> &threads)
{
	save_time();
	for (size_t i = 0; i < threads.size(); i++)
		threads[i] = std::thread(task, std::ref(source_parts[i]), ref(result_parts[i]));
	join_threads(threads);
	save_time();
	return get_delta_time();
}

void threads_measurer::join_threads(std::vector<std::thread>& threads)
{
	for (size_t i = 0; i < threads.size(); i++)
		threads[i].join();
}
