#pragma once
#include <chrono>
#include <functional>
#include "time_table.h"
#include "array_splitter.h"

template<typename T>
using task = std::function<void(const T &source_interval, T &result_interval)>;

class measurer
{
public:
	virtual time_table measure(const task<part_for_thread>& task) = 0;
	virtual time_table measure(const std::vector<task<std::vector<int>>>& tasks) = 0;

protected:
	measurer(std::vector<size_t>::const_iterator arr_sizes_first, std::vector<size_t>::const_iterator arr_sizes_last,
		int num_of_rep,
		int max_value);
	
	void fill_arr(std::vector<int>& arr);
	void save_time();
	std::chrono::duration<double> get_delta_time();

	std::vector<size_t> arr_sizes;
	int num_of_rep, max_value;

private:
	std::chrono::time_point<std::chrono::steady_clock> start_time, finish_time;
	int save_counter = 0;

	std::chrono::time_point<std::chrono::steady_clock> get_now();
};

