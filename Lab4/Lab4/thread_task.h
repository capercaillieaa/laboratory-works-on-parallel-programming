#pragma once
#include <mutex>
#include "array_splitter.h"

class thread_task
{
public:
	virtual void operator()(part_for_thread& source_interval, std::mutex& mut) {};

};

