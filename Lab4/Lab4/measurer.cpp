#include "measurer.h"
#include <numeric>

measurer::measurer(std::vector<size_t>::const_iterator arr_sizes_first, std::vector<size_t>::const_iterator arr_sizes_last, int num_of_rep, int max_value)
{
	arr_sizes = std::vector<size_t>(arr_sizes_first, arr_sizes_last);
	this->num_of_rep = num_of_rep;
	this->max_value = max_value;
}

void measurer::fill_arr(std::vector<int>& arr)
{
	auto size = arr.size();
	for (int i = 0; i < size; i++) {
		arr[i] = (size - i) % max_value;
	}
}

void measurer::save_time()
{
	auto now = get_now();
	if (save_counter % 2 == 0)
		start_time = now;
	else
		finish_time = now;
	save_counter++;
}

std::chrono::duration<double> measurer::get_delta_time()
{
	return finish_time - start_time;
}

std::chrono::time_point<std::chrono::steady_clock> measurer::get_now()
{
	return std::chrono::high_resolution_clock::now();
}
