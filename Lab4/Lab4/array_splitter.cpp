#pragma once
#include "array_splitter.h"

array_splitter::array_splitter(std::vector<int>::iterator first, std::vector<int>::iterator last) :
	arr(first, last), arr_size(arr.size()) {
}

parts_for_threads array_splitter::split(int num_of_parts) {
	auto [part_size, num_of_res] = get_part_size(num_of_parts);
	auto parts = get_parts(num_of_parts, part_size, num_of_res);
	return parts;
}

std::tuple<int, int> array_splitter::get_part_size(int num_of_parts) {
	return { arr_size / num_of_parts, arr_size % num_of_parts };
}

parts_for_threads array_splitter::get_parts(int num_of_parts, int part_size, int num_of_res) {
	auto parts = parts_for_threads();
	auto start_part_ptr = arr.begin();
	for (int i = 0; i < num_of_parts; i++) {
		auto end_part_ptr = start_part_ptr + part_size;
		if (num_of_res-- > 0)
			end_part_ptr++;
		parts.push_back({ start_part_ptr, end_part_ptr });
		start_part_ptr = end_part_ptr;
	}
	return parts;
}