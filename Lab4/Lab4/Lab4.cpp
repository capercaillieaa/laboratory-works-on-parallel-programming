﻿#include <iostream>
#include <numeric>
#include <mutex>
#include <algorithm>
#include <execution>
#include "threads_measurer.h"
#include "exPo_measurer.h"
#include "time_table.h"
#include "table_printer.h"

//TASK_1
///////////////////////////////////////////////////////////////////////////////////////
std::mutex sum_mutex;
long long sum = 0;
void get_sum_with_threads(const part_for_thread& source_interval, part_for_thread& result_interval) {
    auto interval_sum = std::reduce(source_interval.first, source_interval.second);
    std::lock_guard<std::mutex> lock(sum_mutex);
    sum += interval_sum;
}

void get_sum_with_seq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    sum = std::reduce(std::execution::seq, source.begin(), source.end());
}

void get_sum_with_par_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    sum = std::reduce(std::execution::par, source.begin(), source.end());
}

void get_sum_with_unseq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    sum = std::reduce(std::execution::par_unseq, source.begin(), source.end());
}

//TASK_2
///////////////////////////////////////////////////////////////////////////////////////
int get_num_of_dividers(int value) {
    if (value == 0) return 0;
    int counter_of_divs = 1;
    for (int i = 1; i <= value / 2; i++) {
        if (value % i == 0)
            counter_of_divs++;
    }
    return counter_of_divs;
}

void get_num_of_dividers_with_threads(const part_for_thread& source_interval, part_for_thread& result_interval) {
    for (auto source_it = source_interval.first, res_it = result_interval.first; source_it != source_interval.second; ++source_it, ++res_it)
        *res_it = get_num_of_dividers(*source_it);
}

void get_num_of_dividers_with_seq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::transform(std::execution::seq, source.begin(), source.end(), result.begin(), get_num_of_dividers);
}

void get_num_of_dividers_with_par_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::transform(std::execution::par, source.begin(), source.end(), result.begin(), get_num_of_dividers);
}

void get_num_of_dividers_with_unseq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::transform(std::execution::par_unseq, source.begin(), source.end(), result.begin(), get_num_of_dividers);
}

//TASK_3
///////////////////////////////////////////////////////////////////////////////////////
int get_gcd(int a, int b) {
    return std::gcd(a, b);
}

void get_gcd_with_threads(const part_for_thread& source_interval, part_for_thread& result_interval) {
    for (auto source_it = source_interval.first, res_it = result_interval.first; source_it != source_interval.second; ++source_it, ++res_it)
        *res_it = get_gcd(*source_it, *res_it);
}

void get_gcd_with_seq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::transform(std::execution::seq, source.begin(), source.end(), result.begin(), result.begin(), get_gcd);
}

void get_gcd_with_par_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::transform(std::execution::par, source.begin(), source.end(), result.begin(), result.begin(), get_gcd);
}

void get_gcd_with_unseq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::transform(std::execution::par_unseq, source.begin(), source.end(), result.begin(), result.begin(), get_gcd);
}

//TASK_4
///////////////////////////////////////////////////////////////////////////////////////
void sort_with_seq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::sort(std::execution::seq, result.begin(), result.end());
}

void sort_with_par_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::sort(std::execution::par, result.begin(), result.end());
}

void sort_with_unseq_ExPo(const std::vector<int>& source, std::vector<int>& result) {
    std::sort(std::execution::par_unseq, result.begin(), result.end());
}

//MAIN
///////////////////////////////////////////////////////////////////////////////////////
template<typename T>
void run_tasks(const std::vector<T>& tasks, measurer &measurer, int num_of_rep, std::string message) {
    std::cout << "-------------------------------------------------------\n";
    std::cout << message << '\n';
    for (size_t i = 0; i < tasks.size(); i++) {
        auto table = measurer.measure(tasks[i]);
        auto times = table.get_times(num_of_rep);
        auto accelerations = table.get_accelerations();
        std::cout << "\n\t\tTask " << i + 1 << "\nTimes:\n";
        table_printer::print(times);
        std::cout << "\nAccelerations:\n";
        table_printer::print(accelerations);
    }
    std::cout << "-------------------------------------------------------\n";
}

int main()
{
    const auto VECTOR_SIZES = std::vector<size_t>{ (size_t)5e6,(size_t)8e6, (size_t)2e7, (size_t)5e7,(size_t)8e7, (size_t)1e8 };
    const auto NUMS_OF_THREADS = std::vector<size_t>{ 1, 2, 4, 8 };
    const auto NUM_OF_REP = 10;
    const auto MAX_VECTOR_VALUE = 10;

    const auto THREAD_TASKS = std::vector<task<part_for_thread>>{ get_sum_with_threads, get_num_of_dividers_with_threads, get_gcd_with_threads };
    const auto EXPO_TASKS = std::vector<std::vector<task<std::vector<int>>>>{
        std::vector<task<std::vector<int>>>{get_sum_with_seq_ExPo, get_sum_with_par_ExPo, get_sum_with_unseq_ExPo},
        std::vector<task<std::vector<int>>>{get_num_of_dividers_with_seq_ExPo, get_num_of_dividers_with_par_ExPo, get_num_of_dividers_with_unseq_ExPo},
        std::vector<task<std::vector<int>>>{get_gcd_with_seq_ExPo, get_gcd_with_par_ExPo, get_gcd_with_unseq_ExPo},
        std::vector<task<std::vector<int>>>{sort_with_seq_ExPo, sort_with_par_ExPo, sort_with_unseq_ExPo}
    };

    threads_measurer threads_measurer(VECTOR_SIZES.cbegin(), VECTOR_SIZES.cend(), NUMS_OF_THREADS.cbegin(), NUMS_OF_THREADS.cend(), NUM_OF_REP, MAX_VECTOR_VALUE);
    exPo_measurer exPo_measurer(VECTOR_SIZES.cbegin(), VECTOR_SIZES.cend(), NUM_OF_REP, MAX_VECTOR_VALUE);
    
    run_tasks<task<part_for_thread>>(THREAD_TASKS, threads_measurer, NUM_OF_REP, "Threads:");
    run_tasks<std::vector<task<std::vector<int>>>>(EXPO_TASKS, exPo_measurer, NUM_OF_REP, "Execution policies:");
}

