#pragma once
#include "measurer.h"
#include <thread>

class threads_measurer : public measurer
{
public:
	threads_measurer(
		std::vector<size_t>::const_iterator arr_sizes_first, std::vector<size_t>::const_iterator arr_sizes_last,
		std::vector<size_t>::const_iterator nums_of_threads_first, std::vector<size_t>::const_iterator nums_of_threads_last,
		int num_of_rep,
		int max_value);

	time_table measure(const task<part_for_thread>& task) override final;
	time_table measure(const std::vector<task<std::vector<int>>>& tasks) override final;

private:
	std::vector<size_t>nums_of_threads;

	void measure_for_current_size(const task<part_for_thread>& task, time_table& times, size_t size, int size_id);
	void measure_for_current_num_of_threads(const task<part_for_thread>& task, time_table& times, std::vector<int>& source_arr, std::vector<int>& result_arr, size_t num_of_threads, int size_id, int thread_id);

	parts_for_threads split_array(std::vector<int>& arr, int num_of_parts);

	std::chrono::duration<double> run_task(const task<part_for_thread>& task, const parts_for_threads& source_parts, parts_for_threads& result_parts, std::vector<std::thread>& threads);
	void join_threads(std::vector<std::thread>& threads);
};

