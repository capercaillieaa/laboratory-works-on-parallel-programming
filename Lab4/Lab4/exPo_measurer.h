#pragma once
#include "measurer.h"

class exPo_measurer : public measurer
{
public:
	exPo_measurer(
		std::vector<size_t>::const_iterator arr_sizes_first, std::vector<size_t>::const_iterator arr_sizes_last,
		int num_of_rep,
		int max_value);

	time_table measure(const task<part_for_thread>& task) override final;
	time_table measure(const std::vector<task<std::vector<int>>>& tasks) override final;
private:
	void measure_for_current_size(const std::vector<task<std::vector<int>>>& tasks, time_table& times, size_t size, int size_id);
	void measure_for_current_exPo(const task<std::vector<int>>& task, time_table& times, std::vector<int>& source_arr, std::vector<int>& result_arr, int size_id, int exPo_id);
	std::chrono::duration<double> run_task(const task<std::vector<int>>& task, std::vector<int>& source_parts, std::vector<int>& result_parts);
};

