#include "exPo_measurer.h"

exPo_measurer::exPo_measurer(
	std::vector<size_t>::const_iterator arr_sizes_first, std::vector<size_t>::const_iterator arr_sizes_last,
	int num_of_rep,
	int max_value) : measurer(arr_sizes_first, arr_sizes_last, num_of_rep, max_value)
{
}

time_table exPo_measurer::measure(const task<part_for_thread>& task)
{
	return time_table(0, 0);
}

time_table exPo_measurer::measure(const std::vector<task<std::vector<int>>>& tasks)
{
	time_table times(arr_sizes.size(), 3);

	for (size_t i = 0; i < arr_sizes.size(); i++)
		measure_for_current_size(tasks, times, arr_sizes[i], i);

	return times;
}

void exPo_measurer::measure_for_current_size(const std::vector<task<std::vector<int>>>& tasks, time_table& times, size_t size, int size_id)
{
	auto source_arr = std::vector<int>(size);
	auto result_arr = std::vector<int>(size);
	fill_arr(source_arr);
	fill_arr(result_arr);

	for (size_t i = 0; i < tasks.size(); i++)
		measure_for_current_exPo(tasks[i], times, source_arr, result_arr, size_id, i);
}

void exPo_measurer::measure_for_current_exPo(const task<std::vector<int>>& task, time_table& times, std::vector<int>& source_arr, std::vector<int>& result_arr, int size_id, int exPo_id)
{
	for (int i = 0; i < num_of_rep; i++) {
		auto delta_time = run_task(task, source_arr, result_arr);
		times.update(size_id, exPo_id, delta_time);
		fill_arr(source_arr);
		fill_arr(result_arr);
	}
}

std::chrono::duration<double> exPo_measurer::run_task(const task<std::vector<int>>& task, std::vector<int>& source_parts, std::vector<int>& result_parts)
{
	save_time();
	task(source_parts, result_parts);
	save_time();
	return get_delta_time();
}
