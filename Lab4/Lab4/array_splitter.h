#pragma once
#include <vector>
#include <tuple>

using part_for_thread = std::pair<std::vector<int>::iterator, std::vector<int>::iterator>;

using parts_for_threads = std::vector<part_for_thread>;

class array_splitter
{
public:
	explicit array_splitter(std::vector<int>::iterator first, std::vector<int>::iterator last);

	parts_for_threads split(int num_of_parts);

private:
	std::vector<int> arr;
	size_t arr_size;

	std::tuple<int, int> get_part_size(int num_of_parts);
	parts_for_threads get_parts(int num_of_parts, int part_size, int num_of_res);
};

