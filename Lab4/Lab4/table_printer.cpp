#include "table_printer.h"
#include <iostream>

void table_printer::print(const table& times)
{
    for (auto row : times) {
        for (auto cell : row)
            std::cout << cell.count() << " ";
        std::cout << '\n';
    }
}
