package task3

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const maxValueInStorage = 100

// Run the third task
func Run() {
	fmt.Println("Please enter the number of writers and readers:")
	var numOfWriters, numOfReaders int
	fmt.Scan(&numOfWriters, &numOfReaders)

	fmt.Println("Please enter the writing and reading maximum time (ms.):")
	var maxWrTime, maxRdTime int
	fmt.Scan(&maxWrTime, &maxRdTime)

	fmt.Println("Please enter the working time (s.):")
	var workTime int
	fmt.Scan(&workTime)

	storageValue := new(int)
	accessMutex := new(sync.RWMutex)

	for i := 0; i < numOfWriters; i++ {
		go runWriter(storageValue, accessMutex, maxWrTime)
	}
	for i := 0; i < numOfReaders; i++ {
		go runReader(storageValue, accessMutex, maxRdTime)
	}

	time.Sleep(time.Duration(workTime) * time.Second)
	fmt.Println("Simulation is finished!")
}

func runWriter(storageValue *int, wrAccessMutex *sync.RWMutex, maxWrTime int) {
	for {
		time.Sleep(time.Duration(rand.Intn(maxWrTime)) * time.Millisecond)

		wrAccessMutex.Lock()
		*storageValue = rand.Intn(maxValueInStorage + 1)
		fmt.Println("The new value", *storageValue, "is written to the storage")
		wrAccessMutex.Unlock()
	}
}

func runReader(storageValue *int, rdAccessMutex *sync.RWMutex, maxRdTime int) {
	for {
		time.Sleep(time.Duration(rand.Intn(maxRdTime)) * time.Millisecond)

		rdAccessMutex.RLock()
		fmt.Println("Value", *storageValue, "is read from the storage")
		rdAccessMutex.RUnlock()
	}
}
