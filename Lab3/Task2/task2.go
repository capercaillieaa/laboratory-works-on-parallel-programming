package task2

import (
	"bufio"
	"errors"
	"io"
	"os"
	"strconv"
	"strings"
)

// Run the second task
func Run() {
	rdFile, err := openRdFile("Task2/arrays.txt")
	if err != nil {
		panic(err)
	}
	wrFile, err := openWrFile("Task2/answer.txt")
	if err != nil {
		panic(err)
	}
	defer rdFile.Close()
	defer wrFile.Close()

	reader := bufio.NewReader(rdFile)
	writer := bufio.NewWriter(wrFile)

	numOfArrays, err := readNumOfArrays(reader)
	if err != nil {
		panic(err)
	}

	rdCh := make(chan []int, numOfArrays)
	wrCh := make(chan int, numOfArrays)
	stopFlagCh := make(chan bool)

	go runReaderWriter(reader, writer, rdCh, wrCh, stopFlagCh)
	go runSumGetter(rdCh, wrCh)
	<-stopFlagCh
}

func openRdFile(fileName string) (*os.File, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, errors.New("couldn't open the file for reading")
	}
	return file, nil
}

func openWrFile(fileName string) (*os.File, error) {
	file, err := os.Create(fileName)
	if err != nil {
		return nil, errors.New("couldn't open the file for writing")
	}
	return file, nil
}

func readNumOfArrays(reader *bufio.Reader) (int, error) {
	str, err := reader.ReadString('\n')
	if err != nil {
		return -1, errors.New("couldn't read the number of arrays")
	}
	numOfArrays, err := convertStringToInt(str)
	if err != nil {
		return -1, err
	}
	return numOfArrays[0], nil
}

func convertStringToInt(strValues ...string) ([]int, error) {
	intValues := make([]int, len(strValues))
	for index, str := range strValues {
		str = strings.TrimSpace(str)
		intResult, err := strconv.Atoi(string(str))
		if err != nil {
			return nil, errors.New("couldn't convert string value to integer")
		}
		intValues[index] = intResult
	}
	return intValues, nil
}

func runReaderWriter(reader *bufio.Reader, writer *bufio.Writer, rdCh chan []int, wrCh chan int, stopFlagCh chan bool) {
	defer close(stopFlagCh)
	err := readArrays(reader, rdCh)
	if err != nil {
		panic(err)
	}
	err = writeSum(writer, wrCh)
	if err != nil {
		panic(err)
	}
}

func readArrays(reader *bufio.Reader, rdCh chan []int) error {
	defer close(rdCh)
	for {
		str, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		} else {
			if err != nil {
				return errors.New("couldn't read array")
			}
		}
		strArray := splitStrIntoElements(str)
		intArray, err := convertStringToInt(strArray...)
		if err != nil {
			return err
		}
		rdCh <- intArray
	}
	return nil
}

func splitStrIntoElements(str string) []string {
	return strings.Split(str, " ")
}

func writeSum(writer *bufio.Writer, wrCh chan int) error {
	for sum := range wrCh {
		str := strconv.Itoa(sum) + "\n"
		_, err := writer.WriteString(str)
		if err != nil {
			return errors.New("couldn't write string to file")
		}
		writer.Flush()
	}
	return nil
}

func runSumGetter(rdCh chan []int, wrCh chan int) {
	defer close(wrCh)
	for array := range rdCh {
		sum := 0
		for _, item := range array {
			sum += item
		}
		wrCh <- sum
	}
}
