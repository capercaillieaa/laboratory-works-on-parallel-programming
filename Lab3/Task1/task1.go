package task1

import (
	"fmt"
	"math/rand"
	"time"
)

// Run the first task
func Run() {
	fmt.Println("Please enter the channel size and number of requests:")
	var chSize, numOfReq int
	fmt.Scan(&chSize, &numOfReq)

	fmt.Println("Please enter the production and consumption maximum time (ms.):")
	var maxProdTime, maxConsTime int
	fmt.Scan(&maxProdTime, &maxConsTime)

	prodConsCh := make(chan int, chSize-1)
	stopFlagCh := make(chan bool)

	go runProducer(prodConsCh, numOfReq, maxProdTime)
	go runConsumer(prodConsCh, maxConsTime, stopFlagCh)
	<-stopFlagCh
}

func runProducer(prodConsCh chan int, numOfReq int, maxProdTime int) {
	defer close(prodConsCh)
	for i := 1; i <= numOfReq; i++ {
		time.Sleep(time.Duration(rand.Intn(maxProdTime)) * time.Millisecond)
		prodConsCh <- i
		fmt.Println("Request number", i, "is produced")
	}
}

func runConsumer(prodConsCh chan int, maxConsTime int, stopFlagCh chan bool) {
	defer close(stopFlagCh)
	for request := range prodConsCh {
		time.Sleep(time.Duration(rand.Intn(maxConsTime)) * time.Millisecond)
		fmt.Println("Reqiest number", request, "is consumed")
	}
}
