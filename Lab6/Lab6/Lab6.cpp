﻿#include <iostream>
#include <omp.h>
#include <functional>
#include <chrono>
#include <numeric>
#include "time_table.h"
#include "table_printer.h"

bool enabled_openmp() {
    bool openmp_flag = false;
    #ifdef _OPENMP
    openmp_flag = true;
    #endif

    return openmp_flag;
}

//TASK_1
///////////////////////////////////////////////////////////////////////////////////////
using ln2_func = std::function<void(int, int)>;

auto const SIZES = std::vector<size_t>{ (size_t)1e7, (size_t)5e7, (size_t)1e8 };
auto const NUMS_OF_THREADS = std::vector<size_t>{ 1, 2, 4, 8 };
auto const NUM_OF_REP = 10;

auto get_now() {
    return std::chrono::high_resolution_clock::now();
}

void get_ln2_with_reduction(int n, int num_of_threads) {
    int i;
    double ln = 0;

    omp_set_num_threads(num_of_threads);
    #pragma omp parallel private(i) firstprivate(n) shared(ln)
    {
        #pragma omp for reduction(+:ln)
        for (i = 1; i < n; i++)
            ln += pow(-1, i - 1) / i;
    }
}

void get_ln2_with_critical(int n, int num_of_threads) {
    int i;
    double ln = 0;
    double part_ln = 0;

    omp_set_num_threads(num_of_threads);
    #pragma omp parallel private(i) firstprivate(n, part_ln) shared(ln)
    {
        #pragma omp for
        for (i = 1; i < n; i++) {
            part_ln += pow(-1, i - 1) / i;
        }
        #pragma omp critical
        ln += part_ln;
    }
}

std::chrono::duration<double> run_task(ln2_func task, int n, int num_of_threads) {
    auto start_time = get_now();
    task(n, num_of_threads);
    auto finish_time = get_now();
    return finish_time - start_time;
}

time_table measure_times(ln2_func task) {
    time_table table(SIZES.size(), NUMS_OF_THREADS.size());

    std::cout << "----------------------------------------------\n";
    for (size_t i = 0; i < SIZES.size(); i++) {
        for (size_t j = 0; j < NUMS_OF_THREADS.size(); j++) {
            for (int k = 0; k < NUM_OF_REP; k++) {
                auto delta_time = run_task(task, SIZES[i], NUMS_OF_THREADS[j]);
                table.update(i, j, delta_time);
            }
            std::cout << "N = " << SIZES[i] << "; Number of threads = " << NUMS_OF_THREADS[j] << "; Completed!\n";
        }
        std::cout << '\n';
    }
    return table;
}

void task_1() {
    auto tasks = std::vector<ln2_func>{ get_ln2_with_reduction, get_ln2_with_critical };

    for (auto task : tasks) {
        auto table = measure_times(task);
        auto times = table.get_times(NUM_OF_REP);
        auto accelerations = table.get_accelerations();
        auto efficiency = table.get_efficiency();

        std::cout << "\nTimes:\n";
        table_printer::print(times);

        std::cout << "\nAccelerations:\n";
        table_printer::print(accelerations);

        std::cout << "\nEfficiency:\n";
        table_printer::print(efficiency);
    }
}

//TASK_2
///////////////////////////////////////////////////////////////////////////////////////
void task_2() {
    std::cout << "Please enter M, K, N: ";
    int m, k, n; 
    std::cin >> m >> k >> n;

    int i = 1, j = 0;
    long long sum = 0;
    double length = 0;

    auto arr = std::vector<int>(n);
    std::iota(arr.begin(), arr.end(), 1);

    omp_set_nested(true);
    if (!omp_get_nested()) {
        std::cout << "Nested parallelism isn't supported!";
        return;
    }

    int tid;
    #pragma omp parallel default(shared) firstprivate(m, k, n, i, j, tid)
    {
        #pragma omp sections
        {
            #pragma omp section
            {
                #pragma omp parallel num_threads(m)
                {
                    tid = omp_get_thread_num();
                    printf("Sum thread id = %d\n", tid);
                    #pragma omp for reduction(+:sum)
                    for (i = 1; i <= n; i++)
                        sum += i;
                }
            }

            #pragma omp section
            {
                #pragma omp parallel num_threads(k)
                {
                    tid = omp_get_thread_num();
                    printf("Length thread id = %d\n", tid);
                    #pragma omp for reduction(+:length)
                    for (int j = 0; j < n; j++)
                        length += pow(arr[j], 2);
                }
            }
        }
    }

    std::cout << "Sum: " << sum << '\n';
    std::cout << "Length: " << sqrt(length) << '\n';
}

int main()
{
    auto openmp_flag = enabled_openmp();
    if (openmp_flag) {
        task_1();
        //task_2();
    }
    else {
        std::cout << "OpenMP isn't supported!";
    }
}

