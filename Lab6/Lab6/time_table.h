#pragma once
#include <vector>
#include <chrono>

using table = std::vector<std::vector<std::chrono::duration<double>>>;

class time_table
{
public:
	time_table(size_t n, size_t m);

	void update(size_t i_pos, size_t j_pos, std::chrono::duration<double> delta_time);
	table get_times(int num_of_rep);
	table get_accelerations();
	table get_efficiency();

private:
	table times;
	size_t n_size, m_size;

	void average_times(int num_of_rep);
};

