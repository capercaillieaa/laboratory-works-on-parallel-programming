#include "time_table.h"

time_table::time_table(size_t n, size_t m) :
	n_size(n), m_size(m), times(n, std::vector<std::chrono::duration<double>>(m)) {

}

void time_table::update(size_t i_pos, size_t j_pos, std::chrono::duration<double> delta_time) {
	times[i_pos][j_pos] += delta_time;
}

table time_table::get_times(int num_of_rep) {
	average_times(num_of_rep);
	return times;
}

table time_table::get_accelerations() {
	auto accelerations = table(n_size);
	for (size_t i = 0; i < n_size; i++)
		for (size_t j = 1; j < m_size; j++)
			accelerations[i].push_back((std::chrono::duration<double>)(times[i][0] / times[i][j]));
	return accelerations;
}

table time_table::get_efficiency()
{
	auto efficiency = table(n_size);
	auto accelerations = get_accelerations();
	for (size_t i = 0; i < n_size; i++)
		for (size_t j = 0; j < m_size - 1; j++)
			efficiency[i].push_back(accelerations[i][j] / pow(2, j + 1));
	return efficiency;
}

void time_table::average_times(int num_of_rep) {
	for (size_t i = 0; i < n_size; i++)
		for (size_t j = 0; j < m_size; j++)
			times[i][j] /= num_of_rep;
}
