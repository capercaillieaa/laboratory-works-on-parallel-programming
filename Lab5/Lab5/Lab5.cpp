﻿#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <chrono>
#include <omp.h>
#include "time_table.h"
#include "table_printer.h"

//TASK_1
///////////////////////////////////////////////////////////////////////////////////////
void task_1() {
    #ifdef _OPENMP   
    printf("OpenMP is supported! %d\n", _OPENMP); 
    #endif
}

//TASK_2
///////////////////////////////////////////////////////////////////////////////////////
void task_2() {
    int nthreads, tid;

    #pragma omp parallel private(tid)
    {        
        tid = omp_get_thread_num();
        printf("Hello World from thread = %d\n", tid);
        if (tid == 0) {
            nthreads = omp_get_num_threads();
            printf("Number of threads = %d\n", nthreads);
        }
    }
}

//TASK_3
///////////////////////////////////////////////////////////////////////////////////////
auto const NUM_OF_REP = 1;

void print_vector(const std::vector<long long>& arr) {
    for (auto item : arr)
        std::cout << item << ' ';
    std::cout << '\n';
}

auto get_now() {
    return std::chrono::high_resolution_clock::now();
}

auto scalar_mul(const std::vector<int>& first_vector, const std::vector<int>& second_vector, std::vector<long long>& result_vector) {
    int n = first_vector.size();
    int i = 0;

    auto start_time = get_now();
    #pragma omp parallel default(shared) firstprivate(i, n)
    {
        #pragma omp for schedule(dynamic, 1000) 
        for (i = 0; i < n; i++) {
            result_vector[i] = (long long)first_vector[i] * second_vector[i];
        }
    }
    auto finish_time = get_now();
    return finish_time - start_time;
}

void fill_vector(std::vector<int>& arr) {
    std::iota(arr.begin(), arr.end(), 1);
}

void task_3(bool print_result) {
    std::cout << "Please enter the length of vectors: ";
    int n; std::cin >> n;

    auto first_vector = std::vector<int>(n);
    auto second_vector = std::vector<int>(n);
    auto result_vector = std::vector<long long>(n);

    fill_vector(first_vector);
    fill_vector(second_vector);

    time_table table(1, 1);
    for (int i = 0; i < NUM_OF_REP; i++) {
        auto delta_time = scalar_mul(first_vector, second_vector, result_vector);
        if (print_result) print_vector(result_vector);
        table.update(0, 0, delta_time);
        std::fill(result_vector.begin(), result_vector.end(), 0);
    }
    auto times = table.get_times(NUM_OF_REP);
    std::cout << "Time: ";
    table_printer::print(times);
}

//TASK_4
///////////////////////////////////////////////////////////////////////////////////////
void task_4() {
    std::cout << "Please enter N: ";
    int n; std::cin >> n;

    int i = 0;
    #pragma omp parallel firstprivate(i, n)
    {
        #pragma omp sections 
        {
            #pragma omp section
                for (i = 1; i <= n; i++)
                    printf("%d\n", i);

            #pragma omp section
                for (i = 0; i < n; i++)
                    printf("HELLO\n");
        }
    }
}

//TASK_5
///////////////////////////////////////////////////////////////////////////////////////
typedef std::vector<std::vector<int>> matrix;

auto const MATRIX_SIZES = std::vector<int>{ 100, 250, 500, 1000 };
auto const NUMS_OF_THREADS = std::vector<int>{ 1, 2, 4, 8 };
auto const MATRIX_VALUE = 2;

void print_matrix(const matrix& matrix) {
    for (auto row : matrix) {
        for (auto cell : row)
            std::cout << cell << ' ';
        std::cout << '\n';
    }
    std::cout << '\n';
}

auto matrix_mul(const matrix& first_matrix, const matrix& second_matrix, matrix& result_matrix, int matrix_size, int num_of_threads) {
    omp_set_num_threads(num_of_threads);
    int i, j, k;

    auto start_time = get_now();
    #pragma omp parallel default(shared) private(i, j, k)
    {
        #pragma omp for schedule(guided, 10)
        for (i = 0; i < matrix_size; i++) {
            for (j = 0; j < matrix_size; j++) {
                for (k = 0; k < matrix_size; k++) {
                    result_matrix[i][j] += (first_matrix[i][k] * second_matrix[k][j]);
                }
            }
        }
    }
    auto finish_time = get_now();
    return finish_time - start_time;
}

matrix gen_matrix(int size, int init_value = MATRIX_VALUE) {
    auto new_matrix = matrix(size, std::vector<int>(size));
    for (int i = 0; i < size; i++)
        std::fill(new_matrix[i].begin(), new_matrix[i].end(), init_value);
    return new_matrix;
}
void task_5(bool print_result) {
    time_table table(MATRIX_SIZES.size(), NUMS_OF_THREADS.size());

    for (size_t i = 0; i < MATRIX_SIZES.size(); i++) {
        auto first_matrix = gen_matrix(MATRIX_SIZES[i]);
        auto second_matrix = gen_matrix(MATRIX_SIZES[i]);
        auto result_matrix = gen_matrix(MATRIX_SIZES[i], 0);
        for (size_t j = 0; j < NUMS_OF_THREADS.size(); j++) {
            for (int k = 0; k < NUM_OF_REP; k++) {
                auto delta_time = matrix_mul(first_matrix, second_matrix, result_matrix, MATRIX_SIZES[i], NUMS_OF_THREADS[j]);
                if (print_result) {
                    print_matrix(first_matrix);
                    print_matrix(second_matrix);
                    print_matrix(result_matrix);
                }
                table.update(i, j, delta_time);
                result_matrix = gen_matrix(MATRIX_SIZES[i], 0);
            }
            std::cout << "Size = " << MATRIX_SIZES[i] << "; Number of threads = " << NUMS_OF_THREADS[j] << "; Completed!\n";
        }
        std::cout << '\n';
    }

    auto times = table.get_times(NUM_OF_REP);
    auto accelerations = table.get_accelerations();

    std::cout << "\nTimes:\n";
    table_printer::print(times);

    std::cout << "\nAccelerations:\n";
    table_printer::print(accelerations);
}

//MAIN
///////////////////////////////////////////////////////////////////////////////////////
int main()
{
    //task_1();
    //task_2();
    //task_3(false);
    //task_4();
    task_5(false);
}

