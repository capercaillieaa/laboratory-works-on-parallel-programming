﻿#include <iostream>
#include <stdio.h>
#include <vector>
#include <functional>
#include <numeric>
#include <ctime>
#include <mpi.h> 

//TASK_1
///////////////////////////////////////////////////////////////////////////////////////
void task_1(int argc, char** argv) {
    int proc_num, proc_rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    printf("I'm %d process from %d processes!", proc_rank, proc_num);

    MPI_Finalize();
}

//TASK_2
///////////////////////////////////////////////////////////////////////////////////////
void task_2(int argc, char** argv) {
    int proc_num, proc_rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);

    if (proc_rank == 0)
        printf("%d processes", proc_num);
    else if (proc_rank % 2 == 0)
        printf("%d process : FIRST!", proc_rank);
    else 
        printf("%d process : SECOND!", proc_rank);
   
    MPI_Finalize();
}

//TASK_3
///////////////////////////////////////////////////////////////////////////////////////
constexpr auto MSGLEN = 32768;
constexpr auto TAG_A = 100; 
constexpr auto TAG_B = 200; 

void task_3(int argc, char** argv) {
    std::vector<int> message1(MSGLEN), message2(MSGLEN);
    int rank, dest, source, send_tag, recv_tag;     
    MPI_Status statuses[2];  
    MPI_Request requests[2];

    MPI_Init(&argc, &argv);     
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   

    for (int i = 0; i < MSGLEN; i++) { 
        message1[i] = 1 - 2 * rank;
    } 

    if (rank == 0) { 
        dest = 1; 
        source = 1; 
        send_tag = TAG_A; 
        recv_tag = TAG_B; 
    }
    else if (rank == 1) { 
        dest = 0; 
        source = 0; 
        send_tag = TAG_B; 
        recv_tag = TAG_A; 
    }     

    std::cout << "Task " << rank << " has sent the message" << std::endl;
    MPI_Isend(message1.data(), MSGLEN, MPI_INT, dest, send_tag, MPI_COMM_WORLD, &requests[0]);
    MPI_Irecv(message2.data(), MSGLEN, MPI_INT, source, recv_tag, MPI_COMM_WORLD, &requests[1]);
    if (MPI_Waitall(2, requests, statuses) == MPI_SUCCESS)
        std::cout << "Task " << rank << " has received the message" << std::endl;
    MPI_Finalize();
}

//TASK_4
///////////////////////////////////////////////////////////////////////////////////////
void task_4(int argc, char** argv) {
    int proc_num, proc_rank;
    int n, next, prev;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);

    next = (proc_rank + 1) % proc_num;
    prev = (proc_rank + proc_num - 1) % proc_num;

    if (proc_rank == 0) {
        srand((unsigned)time(NULL));
        n = rand() % 100;
        printf("0 generated number %d\n", n);
        int expected = n + proc_num - 1;

        MPI_Send(&n, 1, MPI_INT, next, 0, MPI_COMM_WORLD);
        MPI_Recv(&n, 1, MPI_INT, prev, 0, MPI_COMM_WORLD, &status);

        if (n == expected)
            printf("Correct!");
        else
            printf("Error!");
    }
    else {
        MPI_Recv(&n, 1, MPI_INT, prev, 0, MPI_COMM_WORLD, &status);
        printf("%d recieve number %d from %d", proc_rank, n, prev);
        n++;
        MPI_Send(&n, 1, MPI_INT, next, 0, MPI_COMM_WORLD);
    }
    MPI_Finalize();
}

//TASK_5
///////////////////////////////////////////////////////////////////////////////////////
long long get_row_sum(const std::vector<int>& row) {
    return (long long)std::reduce(row.begin(), row.end());
}

long long blocking_get_chunk_sum(const std::vector<std::vector<int>>& matrix, int chunk_size, int offset, int proc_count, bool is_last_chunk) {
    long long chunk_sum = 0, row_sum;
    auto row_size = matrix.front().size();
    std::vector<int> empty_row(row_size, 0);
    MPI_Status status;
    for (int i = 1; i < proc_count; i++) {
        auto row_data = i <= chunk_size ? matrix[offset + i - 1].data() : empty_row.data();
        MPI_Send(&is_last_chunk, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
        MPI_Send(row_data, row_size, MPI_INT, i, 0, MPI_COMM_WORLD);
        MPI_Recv(&row_sum, 1, MPI_LONG_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
        chunk_sum += row_sum;
    }
    return chunk_sum;
}

long long non_blocking_get_chunk_sum(const std::vector<std::vector<int>>& matrix, int chunk_size, int offset, int proc_count, bool is_last_chunk) {
    long long chunk_sum = 0, row_sum;
    auto row_size = matrix.front().size();
    std::vector<int> empty_row(row_size, 0);
    MPI_Request requests[3];
    MPI_Status statuses[3];
    for (int i = 1; i < proc_count; i++) {
        auto row_data = i <= chunk_size ? matrix[offset + i - 1].data() : empty_row.data();
        MPI_Isend(&is_last_chunk, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD, &requests[0]);
        MPI_Isend(row_data, row_size, MPI_INT, i, 0, MPI_COMM_WORLD, &requests[1]);
        MPI_Irecv(&row_sum, 1, MPI_LONG_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &requests[2]);
        MPI_Waitall(3, requests, statuses);
        chunk_sum += row_sum;
    }
    return chunk_sum;
}

void blocking_send_message_size(int proc_count, int size) {
    for (int i = 1; i < proc_count; i++)
        MPI_Send(&size, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
}

void non_blocking_send_message_size(int proc_count, int size) {
    MPI_Request request;
    for (int i = 1; i < proc_count; i++)
        MPI_Isend(&size, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &request);
}

void fill_matrix(std::vector<std::vector<int>>& matrix, int n, int m, std::function<int(int, int)> init_func) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            matrix[i][j] = init_func(i, j);
}

void scan_input_data(int* n, int* m) {
    std::cout << "Please input N: ";
    std::cin >> *n;
    std::cout << "Please input M: ";
    std::cin >> *m;
}

void blocking_task_5(int argc, char* argv[]) {
    int proc_rank, proc_count;
    int n, m;
    bool is_last_chunk;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);

    if (proc_rank == 0) {
        scan_input_data(&n, &m);

        std::vector<std::vector<int>> matrix(n, std::vector<int>(m));
        auto init_func = std::plus<>();
        fill_matrix(matrix, n, m, init_func);

        double start_time, finish_time;
        start_time = MPI_Wtime();

        blocking_send_message_size(proc_count, m);

        long long chunk_sum = 0, sum = 0;
        int rem_rows_count = n, offset = 0;
        int chunk_size;
        do {
            is_last_chunk = rem_rows_count <= proc_count - 1;
            chunk_size = std::min(proc_count - 1, rem_rows_count);
            chunk_sum = blocking_get_chunk_sum(matrix, chunk_size, offset, proc_count, is_last_chunk);
            sum += chunk_sum;
            rem_rows_count -= chunk_size;
            offset += chunk_size;
        } while (!is_last_chunk);

        finish_time = MPI_Wtime();

        std::cout << "Sum = " << sum << '\n';
        std::cout << "Time: " << finish_time - start_time << '\n';
    } 
    else {
        MPI_Recv(&m, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        std::vector<int> matrix_row(m);

        long long row_sum = 0;
        while (true) {
            MPI_Recv(&is_last_chunk, 1, MPI_C_BOOL, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(matrix_row.data(), m, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            row_sum = get_row_sum(matrix_row);
            MPI_Send(&row_sum, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
            if (is_last_chunk) break;
        }
    }
    MPI_Finalize();
}

void non_blocking_task_5(int argc, char* argv[]) {
    int proc_rank, proc_count;
    int n, m;
    bool is_last_chunk;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);

    if (proc_rank == 0) {
        scan_input_data(&n, &m);

        std::vector<std::vector<int>> matrix(n, std::vector<int>(m));
        auto init_func = std::plus<>();
        fill_matrix(matrix, n, m, init_func);

        double start_time, finish_time;
        start_time = MPI_Wtime();

        non_blocking_send_message_size(proc_count, m);

        long long chunk_sum = 0, sum = 0;
        int rem_rows_count = n, offset = 0;
        int chunk_size;
        do {
            is_last_chunk = rem_rows_count <= proc_count - 1;
            chunk_size = std::min(proc_count - 1, rem_rows_count);
            chunk_sum = non_blocking_get_chunk_sum(matrix, chunk_size, offset, proc_count, is_last_chunk);
            sum += chunk_sum;
            rem_rows_count -= chunk_size;
            offset += chunk_size;
        } while (!is_last_chunk);

        finish_time = MPI_Wtime();

        std::cout << "Sum = " << sum << '\n';
        std::cout << "Time: " << finish_time - start_time << '\n';
    }
    else {
        MPI_Request requests[2];
        MPI_Status statuses[2];
        MPI_Irecv(&m, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &requests[0]);
        MPI_Wait(&requests[0], &statuses[0]);
        std::vector<int> matrix_row(m);

        long long row_sum = 0;
        while (true) {
            MPI_Irecv(&is_last_chunk, 1, MPI_C_BOOL, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &requests[0]);
            MPI_Irecv(matrix_row.data(), m, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &requests[1]);
            MPI_Waitall(2, requests, statuses);
            row_sum = get_row_sum(matrix_row);
            MPI_Isend(&row_sum, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, &requests[0]);
            if (is_last_chunk) break;
        }
    }
    MPI_Finalize();
}

//TASK_6
///////////////////////////////////////////////////////////////////////////////////////
const int P = 8;

void print_arr(const std::vector<int>& row, int arr_num) {
    printf("%d: ", arr_num);
    for (int val : row)
        printf("%4d ", val);
    printf("\n");
}

void task_6(int argc, char* argv[]) {
    int proc_rank, proc_count;

    MPI_Status st;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);

    std::vector<int> row(P);

    for (int i = 0; i < proc_count; i++)
        row[i] = i + proc_rank * proc_count + 1;

    for (int i = 0; i < proc_count; i++) {
        if (i != proc_rank)
            MPI_Sendrecv_replace(&row[i], 1, MPI_INT, i, 0, i, MPI_ANY_TAG, MPI_COMM_WORLD, &st);
    }

    MPI_Finalize();

    print_arr(row, proc_rank);
}

//MAIN
///////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char**argv) {
    //task_1(argc, argv);
    //task_2(argc, argv);
    //task_3(argc, argv);
    task_4(argc, argv);
    //blocking_task_5(argc, argv);
    //non_blocking_task_5(argc, argv);
    //task_6(argc, argv);
} 
